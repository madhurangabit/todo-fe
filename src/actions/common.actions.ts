import {
  SUCCESS_CLOSE,
  SUCCESS_OPEN,
  SET_SUCCESS_MESSAGE,
  ERROR_OPEN,
  ERROR_CLOSE,
  SET_ERROR_MESSAGE,
  GET_TODOS,
  START_LOADING,
  END_LOADING,
  ADD_TODO,
  DELETE_TODO,
  UPDATE_TODO_STATUS,
} from "../constants/common-constant";

export const startLoading = () => ({
  type: START_LOADING,
});

export const endLoading = () => ({
  type: END_LOADING,
});

export const successOpen = () => ({
  type: SUCCESS_OPEN,
});

export const successClose = () => ({
  type: SUCCESS_CLOSE,
});

export const errorOpen = () => ({
  type: ERROR_OPEN,
});

export const errorClose = () => ({
  type: ERROR_CLOSE,
});

export const setSuccessMessage = (payload: Object) => ({
  type: SET_SUCCESS_MESSAGE,
  payload,
});

export const setErrorMessage = (payload: Object) => ({
  type: SET_ERROR_MESSAGE,
  payload,
});

export const getTodos = (payload: Object) => ({
  type: GET_TODOS,
  payload,
});

export const addTodo = (
  payload: Object,
  success: () => void,
  failed: () => void
) => ({
  type: ADD_TODO,
  payload,
  success,
  failed,
});

export const deleteTodo = (
  payload: Object,
  success?: () => void,
  failed?: () => void
) => ({
  type: DELETE_TODO,
  payload,
  success,
  failed,
});

export const updateTodoStatus = (
  payload: Object,
  success?: () => void,
  failed?: () => void
) => ({
  type: UPDATE_TODO_STATUS,
  payload,
  success,
  failed,
});
