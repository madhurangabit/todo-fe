import logo from "./logo.svg";
import "./App.css";
import Routing from "./routes";
import { BrowserRouter as Router } from "react-router-dom";

import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { persistor, store } from "./store/store";

const App = () => {
  return (
      <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Router>
          <Routing />
        </Router>
      </PersistGate>
    </Provider>
  );
};

export default App;
