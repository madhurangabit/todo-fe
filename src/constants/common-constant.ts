
// common actions
export const START_LOADING = 'START_LOADING';
export const END_LOADING = 'END_LOADING';
export const RELOAD = 'RELOAD';
export const CLEAR_REDUX_STORE = 'CLEAR_REDUX_STORE';


export const SUCCESS_OPEN = 'SUCCESS_OPEN';
export const SUCCESS_CLOSE = 'SUCCESS_CLOSE';
export const SET_SUCCESS_MESSAGE = 'SET_SUCCESS_MESSAGE';
export const ERROR_OPEN = 'ERROR_OPEN';
export const ERROR_CLOSE = 'ERROR_CLOSE';
export const SET_ERROR_MESSAGE = 'SET_ERROR_MESSAGE';

export const GET_TODOS = 'GET_TODOS';
export const ADD_TODO = 'ADD_TODO';
export const GET_TODOS_SUCCESS = 'GET_TODOS_SUCCESS';
export const DELETE_TODO = 'DELETE_TODO';
export const UPDATE_TODO_STATUS = 'UPDATE_TODO_STATUS';



