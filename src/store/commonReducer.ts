import {
  START_LOADING,
  END_LOADING,
  SET_SUCCESS_MESSAGE,
  SET_ERROR_MESSAGE,
  ERROR_CLOSE,
  ERROR_OPEN,
  SUCCESS_OPEN,
  SUCCESS_CLOSE,
  GET_TODOS_SUCCESS,
} from "../constants/common-constant";

export interface ICommonReducer {
  reload: boolean;
  loading: boolean;
  success: boolean;
  successMessage: any;
  error: boolean;
  errorMessage: any;
  todos: [];
}

const initialState: ICommonReducer = {
  reload: false,
  loading: false,
  success: false,
  successMessage: null,
  error: false,
  errorMessage: null,
  todos: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case START_LOADING:
      return {
        ...state,
        loading: true,
      };
    case END_LOADING:
      return {
        ...state,
        loading: false,
      };
    case SUCCESS_OPEN:
      return {
        ...state,
        success: true,
      };

    case SUCCESS_CLOSE:
      return {
        ...state,
        success: false,
      };
    case ERROR_OPEN:
      return {
        ...state,
        error: true,
      };
    case ERROR_CLOSE:
      return {
        ...state,
        error: false,
      };
    case SET_SUCCESS_MESSAGE:
      return {
        ...state,
        success: true,
        successMessage: action.payload,
      };
    case GET_TODOS_SUCCESS:
      return {
        ...state,
        todos: action.payload,
      };
    case SET_ERROR_MESSAGE:
      return {
        ...state,
        error: true,
        errorMessage: action.payload,
      };

    default:
      return state;
  }
};
