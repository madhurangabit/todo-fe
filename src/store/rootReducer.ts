import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { CLEAR_REDUX_STORE } from "../constants/common-constant";

import common from "./commonReducer";

const rootPersistConfig = {
  key: "root",
  storage: storage,
  blacklist: ["common"],
};

const appReducer = combineReducers({
  common,
});

const rootReducer = (state, action) => {
  if (action.type === CLEAR_REDUX_STORE) {
    state = undefined;
    storage.removeItem("persist:root");
  }

  return appReducer(state, action);
};

export default persistReducer(rootPersistConfig, rootReducer);
