import React, { useState } from "react";
import { Card, Typography, Tag, Switch, Button } from "antd";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { COLORS } from "../../constants/colors";

import {
  getTodos,
  deleteTodo,
  updateTodoStatus,
} from "../../actions/common.actions";

const { Title } = Typography;

const CustomCard = styled(Card)`
  border-radius: 3px;
  .ant-card-body {
    padding: 20px;
  }
`;

const CustomTag = styled(Tag)`
  margin-left: 5px;
`;

const CommonTitle = styled(Title)`
  color: ${COLORS.PRIMARY_TEXT} !important;
  font-size: 12px !important;
`;

const PrimaryDetailsContent = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: 1rem;
  @media (max-width: 368px) {
    flex-direction: column;
  }
`;

const CommonRow = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const InfoCard = ({ record, sortedBy }: InfoCardProps) => {
  const dispatch = useDispatch();

  const handleTag = (value: boolean) => {
    if (value === true) {
      return <CustomTag color="green">Active</CustomTag>;
    }
    return <CustomTag color="volcano">Inactive</CustomTag>;
  };

  const toggleSwitch = (checked, record) => {
    const id = record._id;
    const status = checked;
    dispatch(
      updateTodoStatus({ id, status }, () => {
        dispatch(getTodos({ sortedBy }));
      })
    );
  };

  const deleteTodoAction = (record: RecordProps) => {
    const id = record._id;
    dispatch(
      deleteTodo(id, () => {
        dispatch(getTodos({ sortedBy }));
      })
    );
  };

  return (
    <CustomCard bordered={false}>
      <PrimaryDetailsContent>
        <CommonRow>
          <CommonTitle>Status </CommonTitle>
          {handleTag(record?.status)}
        </CommonRow>
        <CommonRow>
          <CommonTitle>Mark As Done </CommonTitle>
          <Switch
            onChange={(checked) => toggleSwitch(checked, record)}
            loading={false}
            defaultChecked={record.status}
          />
        </CommonRow>

        <CommonRow>
          <CommonTitle>Delete </CommonTitle>
          <Button onClick={() => deleteTodoAction(record)} size="middle">
            Delete
          </Button>
        </CommonRow>
      </PrimaryDetailsContent>
    </CustomCard>
  );
};

interface RecordProps {
  _id: String;
  title: String;
  status?: boolean;
}

interface InfoCardProps {
  record: RecordProps;
  sortedBy: number;
}

export default InfoCard;
