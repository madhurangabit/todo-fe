import { Outlet, useNavigate } from "react-router-dom";
import React, { useState, useEffect } from "react";
import {
  Layout,
  Menu,
  notification,
  Avatar,
  Typography,
} from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  PieChartOutlined,
} from "@ant-design/icons";
import { useSelector, useDispatch } from "react-redux";
import { successClose, errorClose } from "../../actions/common.actions";
import { COLORS } from "../../constants/colors";
import { ICommonReducer } from "../../store/commonReducer";
import styled from "styled-components";

export interface CommonReducerTypes {
  common: ICommonReducer;
}

const { Header, Sider, Content } = Layout;
const { Title } = Typography;

const MainLayout = styled(Layout)`
  min-height: 100vh;
`;

const HeaderTitle = styled(Title)`
  color: ${COLORS.PRIMARY} !important;
  margin: 0px !important;
`;

const CustomContent = styled(Content)`
  padding: 24px;
  background-color: ${COLORS.GREY};
  min-height: 100vh;
`;

const TitleContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const CustomHeader = styled(Header)`
  background: ${COLORS.WHITE} !important;
  padding: 0px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const ItemContent = styled.div`
  margin-right: 2rem;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const CustomSlider = styled(Sider)`
  background-color: ${COLORS.WHITE};
  padding: 0.8rem;
`;

const DashboardLayout = () => {
  const dispatch = useDispatch();
  const success = useSelector(
    ({ common }: CommonReducerTypes) => common.success
  );
  const [sidebarCollapsed, setSidebarCollapsed] = useState(false);
  const successMessage = useSelector(
    ({ common }: CommonReducerTypes) => common.successMessage
  );
  const errorMessage = useSelector(
    ({ common }: CommonReducerTypes) => common.errorMessage
  );
  const error = useSelector(({ common }: CommonReducerTypes) => common.error);

  useEffect(() => {
    if (success === true) {
      openNotificationWithSuccessIcon("success");
    }
  }, [success]);

  useEffect(() => {
    if (error === true) {
      openNotificationWithErrorIcon("error");
    }
  }, [error]);

  const openNotificationWithSuccessIcon = (type) => {
    notification[type]({
      message: successMessage,
      onClose: handleSuccess,
    });
  };

  const handleSuccess = () => {
    dispatch(successClose());
  };

  const openNotificationWithErrorIcon = (type) => {
    notification[type]({
      message: errorMessage,
      onClose: handleError,
    });
  };

  const handleError = () => {
    dispatch(errorClose());
  };
  const toggle = () => {
    setSidebarCollapsed(!sidebarCollapsed);
  };

  return (
    <>
      <MainLayout>
        <CustomSlider
          trigger={null}
          collapsible
          collapsed={sidebarCollapsed}
          width={300}
        >
          {/* <div className="logo" /> */}
          <Menu theme="light" defaultSelectedKeys={["1"]} mode="inline">
            <Menu.Item
              key="1"
              icon={<PieChartOutlined style={{ fontSize: "18px" }} />}
            >
              Todo
            </Menu.Item>
          </Menu>
        </CustomSlider>
        <Layout className="site-layout">
          <CustomHeader>
            <TitleContent>
              {React.createElement(
                sidebarCollapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: toggle,
                }
              )}
            </TitleContent>
            <ItemContent>
              <Avatar src="https://www.shareicon.net/data/512x512/2016/09/15/829466_man_512x512.png" />
            </ItemContent>
          </CustomHeader>
          <CustomContent>
            <Outlet />
          </CustomContent>
        </Layout>
      </MainLayout>
    </>
  );
};

export default DashboardLayout;
