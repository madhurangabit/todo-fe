import React, { useEffect, useState } from "react";
import moment from "moment";
import styled from "styled-components";
import {
  Table,
  Typography,
  Modal,
  DatePicker,
  Input,
  Button,
  Row,
  Col,
  Spin,
  Card,
  Radio,
} from "antd";

import { InfoCard } from "./sharedComponents";
import { useSelector, useDispatch } from "react-redux";
import { ICommonReducer } from "../store/commonReducer";
import { getTodos, addTodo } from "../actions/common.actions";
const { Paragraph } = Typography;

export interface CommonReducerTypes {
  common: ICommonReducer;
}

const SpinContent = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const TopButtonContainer = styled.div`
  margin-bottom: 1rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const CustomDatePicker = styled(DatePicker)`
  margin-bottom: 1rem;
`;

const IntroPage = () => {
  const dispatch = useDispatch();
  const loading = useSelector(
    ({ common }: CommonReducerTypes) => common.loading
  );
  const todos = useSelector(({ common }: CommonReducerTypes) => common.todos);

  const [visible, setVisible] = useState<boolean>(false);
  const [sortedBy, setSortedBy] = useState<number>(1);
  const [confirmLoading, setConfirmLoading] = useState<boolean>(false);
  const [todo, setTodo] = useState<String>("");
  const [endDate, setEndDate] = useState<String>("");

  useEffect(() => {
    dispatch(getTodos({ sortedBy }));
  }, []);

  useEffect(() => {
    dispatch(getTodos({ sortedBy }));
  }, [sortedBy]);

  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
      render: (text: String) => <Paragraph>{text}</Paragraph>,
    },
    {
      title: "End Date",
      dataIndex: "endDate",
      responsive: ["sm"],
      key: "endDate",
      render: (date: String) => (
        <Paragraph>{moment(date).format("YYYY-MM-DD")}</Paragraph>
      ),
    },
  ];

  const showAddTodoModal = () => {
    setVisible(true);
  };

  const todoSubmit = () => {
    setConfirmLoading(true);
    dispatch(
      addTodo(
        { title: todo, endDate },
        () => {
          setConfirmLoading(false);
          setVisible(false);
          dispatch(getTodos({ sortedBy }));
        },
        () => {
          setConfirmLoading(false);
          setVisible(false);
        }
      )
    );
  };

  const addTodoModalCancel = () => {
    setVisible(false);
  };

  const expandedRowRender = (record: TodoProps) => {
    return <InfoCard sortedBy={sortedBy} record={record} />;
  };

  const onDateChange = (date: String, dateString: String) => {
    setEndDate(dateString);
  };

  const handleSortedBy = (e:any) => {
    setSortedBy(e.target.value);
  };

  const addTodoModal = () => (
    <Modal
      title="Add todo"
      visible={visible}
      onOk={todoSubmit}
      okButtonProps={{ disabled: todo.length === 0 || endDate.length === 0 }}
      confirmLoading={confirmLoading}
      onCancel={addTodoModalCancel}
    >
      <CustomDatePicker placeholder="End Date" onChange={onDateChange} />
      <Input
        onChange={(e) => setTodo(e.target.value)}
        value={todo}
        placeholder="Add Your Todo"
      />
    </Modal>
  );
  return (
    <Row>
      <Col xs={24} sm={24} md={24} lg={24} xl={24}>
        {loading ? (
          <SpinContent>
            <Spin />
          </SpinContent>
        ) : (
          <div>
            <TopButtonContainer>
              <Button onClick={showAddTodoModal} type="primary" size="middle">
                Add Todo
              </Button>
              <Radio.Group
                size="middle"
                value={sortedBy}
                onChange={handleSortedBy}
              >
                <Radio.Button value={1}>Ascending</Radio.Button>
                <Radio.Button value={-1}>Descending</Radio.Button>
              </Radio.Group>
            </TopButtonContainer>
            <Card title="Todos">
              {todos.length !== 0 && (
                <Table
                  rowKey="_id"
                  columns={columns}
                  dataSource={todos}
                  expandable={{ expandedRowRender }}
                />
              )}
            </Card>
          </div>
        )}
        {addTodoModal()}
      </Col>
    </Row>
  );
};

interface TodoProps {
  _id: String;
  title: String;
  status?: boolean;
}

export default IntroPage;
