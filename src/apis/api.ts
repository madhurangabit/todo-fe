import Axios, { AxiosRequestConfig } from "axios";
import { store } from "../store/store";
import { startLoading, endLoading } from "../actions/common.actions";

interface IRequest {
  url?: string;
  data?: object;
  params?: object;
  action: string;
  guest?: boolean;
  method?: AxiosRequestConfig["method"];
  headers?: AxiosRequestConfig["headers"];
}

const createPath = (action: string) => {
  const url = `${process.env.REACT_APP_BASE_URL}${action}`;
  
  return { url };
};

const request = ({
  url,
  data,
  params,
  action,
  method,
  headers: addHeaders,
}: IRequest) =>
  new Promise((resolve, reject) => {
    store.dispatch(startLoading());
    const headers = {
      "Content-Type": "application/json",
      ...addHeaders,
    };
    const customUrl = createPath(action);
    const axiosConfig: AxiosRequestConfig = {
      data,
      params,
      headers,
      timeout: 30000,
      method: method || "post",
      url: url || customUrl.url,
    };

    Axios(axiosConfig)
      .then((response) => {
        resolve(response.data);
        store.dispatch(endLoading());
      })
      .catch((error) => {
        store.dispatch(endLoading());
        reject(error?.response?.data || error);
      });
  });

export default { request };
