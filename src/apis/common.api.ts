import Api from "./api";

export default class CommonAPI {
  static getTodos = (sortedBy) =>
    Api.request({
      method: "get",
      action: `todo?sortedBy=${sortedBy}`,
    });

  static addTodoAPI = (data: Object) =>
    Api.request({
      method: "post",
      action: "todo",
      data,
    });

  static deleteTodoAPI = (id: String) =>
    Api.request({
      method: "delete",
      action: `todo/${id}`,
    });

  static updateTodoStatusAPI = ({ id, status }) =>
    Api.request({
      method: "put",
      action: `todo/${id}`,
      data: {
        status,
      },
    });
}
