import React, { useState, useEffect } from "react";
import { useRoutes } from "react-router-dom";
import { TodoPage } from "./components";
import { DashboardLayout } from "./components/sharedComponents";

const RouteLayout = () => {
  const routes = useRoutes([
    {
      path: "/",
      element: <DashboardLayout />,
      children: [
        { path: "/", element: <TodoPage /> },
        { path: "todoPage", element: <TodoPage /> },
      ],
    },
  ]);

  return routes;
};
export default RouteLayout;
