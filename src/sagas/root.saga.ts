import { all } from "redux-saga/effects";

import CommonSaga from "./common.saga";

export default function* rootSaga() {
  yield all([CommonSaga()]);
}
