import { put, call, takeEvery } from "redux-saga/effects";
import {
  GET_TODOS,
  ADD_TODO,
  GET_TODOS_SUCCESS,
  SET_ERROR_MESSAGE,
  SET_SUCCESS_MESSAGE,
  DELETE_TODO,
  UPDATE_TODO_STATUS,
} from "../constants/common-constant";
import CommonAPI from "../apis/common.api";

export type TSaga = {
  type: string;
  payload?: any;
  success?: Function;
  failed?: Function;
  hideLoader?: boolean;
};

export function* getTodos({ payload }: TSaga) {
  try {
    const { sortedBy } = payload;
    const data = yield call(CommonAPI.getTodos, sortedBy);
    yield put({ type: GET_TODOS_SUCCESS, payload: data });
    const message = "Todos Fetched successfully!";
    yield put({ type: SET_SUCCESS_MESSAGE, payload: message });
  } catch (error) {
    const message = "Something Went wrong!";
    yield put({ type: SET_ERROR_MESSAGE, payload: message });
  }
}

export function* addTodo({ payload, success, failed }: TSaga) {
  try {
    const data = yield call(CommonAPI.addTodoAPI, payload);
    success();
    const message = "Todo Added successfully!";
    yield put({ type: SET_SUCCESS_MESSAGE, payload: message });
  } catch (error) {
    const message = "Something Went wrong!";
    yield put({ type: SET_ERROR_MESSAGE, payload: message });
    failed();
  }
}

export function* deleteTodo({ payload, success, failed }: TSaga) {
  try {
    const data = yield call(CommonAPI.deleteTodoAPI, payload);
    success();
    const message = "Todo Deleted successfully!";
    yield put({ type: SET_SUCCESS_MESSAGE, payload: message });
  } catch (error) {
    const message = "Something Went wrong!";
    yield put({ type: SET_ERROR_MESSAGE, payload: message });
    failed();
  }
}

export function* updateTodoStatus({ payload, success, failed }: TSaga) {
  try {
    const { id, status } = payload;
    const data = yield call(CommonAPI.updateTodoStatusAPI, { id, status });
    success();
    const message = "Todo Updated successfully!";
    yield put({ type: SET_SUCCESS_MESSAGE, payload: message });
  } catch (error) {
    const message = "Something Went wrong!";
    yield put({ type: SET_ERROR_MESSAGE, payload: message });
    failed();
  }
}

function* commonSaga() {
  yield takeEvery(GET_TODOS, getTodos);
  yield takeEvery(ADD_TODO, addTodo);
  yield takeEvery(DELETE_TODO, deleteTodo);
  yield takeEvery(UPDATE_TODO_STATUS, updateTodoStatus);
}

export default commonSaga;
